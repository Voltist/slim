var scrolly;
var doned = 0;
var items = [];
var index = 0;

function next() {
  if (doned != items.length) {
    if (index == items.length) {
      index = 0;
    }

    

  } else {
    $("#content").html("<h1>Finished!</h1>");
  }
}

function move_top() {
  $('.centerAll').css({
    'position': 'absolute',
    'left': '50%',
    'top': '2%',
    'margin-left': function() {
      return -$(this).outerWidth() / 2
    },
    'margin-top': function() {
      return -$(this).outerHeight() / 2
    }
  });
}

function getwith(u) {
  $.ajax({
      method: "POST",
      url: u,
      data: {
        search: $("#search").val()
      }
    })
    .done(function(data) {

      if (data["success"]) {
        move_top();
        doned = 0;
        items = data["data"];
        index = 0;
      } else {
        $("#content").html("<h6>An unknown error occured, please try again soon</h6>");
        console.log(data["error"]);
      }

    });
}

function search() {
  $.ajax('http://localhost:8080/api/tester', {method: "POST"})
      .done(function() {
        getwith("http://localhost:8080/api/learn");
      })
      .fail(function() {
        getwith("https://slim.artifxl.com/api/learn");
      });
}


$(function() {
  $('.centerAll').css({
    'position': 'absolute',
    'left': '50%',
    'top': '50%',
    'margin-left': function() {
      return -$(this).outerWidth() / 2
    },
    'margin-top': function() {
      return -$(this).outerHeight() / 2
    }
  });

  $('.centerBottom').css({
    'position': 'fixed',
    'left': '50%',
    'top': '99%',
    'margin-left': function() {
      return -$(this).outerWidth() / 2
    },
    'margin-top': function() {
      return -$(this).outerHeight() / 2
    }
  });
  scrolly = new SmoothScroll('a[href*="#"]');

});

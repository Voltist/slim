var scrolly;

function search() {
  $.ajax('http://localhost:8080/api/tester', {method: "POST"})
      .done(function() {
        search_with("http://localhost:8080/api/search");
      })
      .fail(function() {
        search_with("https://slim.artifxl.com/api/search");
      });
}

function search_with(u) {
  if ($("#search").val() != "") {
    $("#content").html(`<div class="spinner">
  <div class="rect1"></div>
  <div class="rect2"></div>
  <div class="rect3"></div>
  <div class="rect4"></div>
  <div class="rect5"></div>
</div>`);

    $.ajax({
        method: "POST",
        url: u,
        data: {
          search: $("#search").val()
        }
      })
      .done(function(data) {

        if (data["success"]) {
          $("#content").html("<span>" + data["result"] + "</span>");
          var anchor = document.querySelector('#content');
          scrolly.animateScroll(anchor);
        } else {
          $("#content").html("<h6>An unknown error occured, please try again soon</h6>");
          console.log(data["error"]);
        }

      });
  }
}

$(function() {
  $('.centerAll').css({
    'position': 'absolute',
    'left': '50%',
    'top': '50%',
    'margin-left': function() {
      return -$(this).outerWidth() / 2
    },
    'margin-top': function() {
      return -$(this).outerHeight() / 2
    }
  });

  $('.centerBottom').css({
    'position': 'fixed',
    'left': '50%',
    'top': '99%',
    'margin-left': function() {
      return -$(this).outerWidth() / 2
    },
    'margin-top': function() {
      return -$(this).outerHeight() / 2
    }
  });

  $('.modal').modal();
  scrolly = new SmoothScroll('a[href*="#"]');
});

// Get the input field
var input = document.getElementById("search");

// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function(event) {
  // Cancel the default action, if needed
  event.preventDefault();
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Trigger the button element with a click
    search();
  }
});

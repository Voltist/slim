$(function() {
  if (localStorage.getItem('slim_password') != null) {
    login(localStorage.getItem('slim_password'));
  } else {
    $("#intro").html(`<h1>Console</h1>

    <div class="input-field container">
      <input placeholder="Password" id="search" type="password" class="center-align">
    </div>
    <button class="btn-small z-depth-0 brand-border hover-dark" onclick="login($('#search').val())" style="width:20%;">Enter</button>
    <label>
        <input type="checkbox" id="remember"/>
        <span>Remember Me</span>
      </label>`);
  }

  $('.centerAll').css({
    'position': 'absolute',
    'left': '50%',
    'top': '50%',
    'margin-left': function() {
      return -$(this).outerWidth() / 2
    },
    'margin-top': function() {
      return -$(this).outerHeight() / 2
    }
  });
});

function login(p) {
  $("#intro").html("<h4><i>Loading...</i></h4>");
  $.ajax({
      method: "POST",
      url: "https://slim.artifxl.com/api/usage",
      data: {
        password: p
      }
    })
    .done(function(data) {
      if (data["success"]) {
        display(data);
        if ($("#remember").is(":checked") != false) {
          localStorage.setItem('slim_password', p);
        }
      } else {
        if (data["error"] == "Incorrect password!!!!!!!") {
          $("#intro").html("<h4><i>Password incorrect, please reload to try again...</i></h4>");
          localStorage.setItem('slim_password', null);
        } else {
          console.log(data["error"]);
          $("#intro").html("<h4><i>An unknown error occured, please check the console for info...</i></h4>");
        }
      }
    });
}

function display(data) {
  $("body").html(`
    <div class="container" style="padding-top: 2%;">
      <div id="stats" class="row">
        <div id="guids" class="col s12 l5 card-panel center-align" style="height: 30vh; width: 49%">
          <h1 id="active_users" style="font-size: 15vh;"></h1>
          <h6>Active Users</h6>
        </div>
        <div style="width: 2%;" class="col s12 l2">
        </div>
        <div id="all" class="col s12 l5 card-panel center-align" style="height: 30vh; width: 49%">
          <h1 id="all_users" style="font-size: 15vh;"></h1>
          <h6>All Installs</h6>
        </div>
      </div>

      <div id="graphs" class="row card-panel">
        <div id="searches" class="col s12 l6" style="height: 50vh;">
        </div>
        <div id="opens" class="col s12 l6" style="height: 50vh;">
        </div>
      </div>
    </div>
    `);

  var trace = {
    x: data["searches"][0],
    y: data["searches"][1],
    mode: 'lines'
  };
  var layout = {
    title: 'Searches per Day'
  };
  Plotly.newPlot('searches', [trace], layout);

  var trace = {
    x: data["opened"][0],
    y: data["opened"][1],
    mode: 'lines'
  };
  var layout = {
    title: 'App Opens per Day'
  };
  Plotly.newPlot('opens', [trace], layout);

  $("#active_users").html(data["guids"]["active"]);
  $("#all_users").html(data["guids"]["all"]);
}

var scrolly;

function getwith(u0, u1) {
  $.ajax({
      method: "POST",
      url: u0,
      data: {}
    })
    .done(function(data) {

      if (data["success"]) {
        var arrayLength = data["data"].length;
        for (var i = 0; i < arrayLength; i++) {
          var item = data["data"][i];
          $("#content").append("<div class='news-item' id='" + item["title"].replace(/\s/g, '').replace(/['"]+/g, '') + "'><a title='Click here to read the full article...' href='" + item["url"] + "'<h6><b>" + item["title"] + "</b></h6></a><p>" + item["source"] + " - "
           + item["time"] + `</p></div> <div class='container'><div class='spinner'>
            <div class='bounce1'></div>
            <div class='bounce2'></div>
            <div class='bounce3'></div>
          </div></div>`);
        }
        var item;

        for (var i = 0; i < arrayLength; i++) {
          item = data["data"][i];

          $.ajax({
              method: "POST",
              url: u1,
              data: {
                url_id: item["title"]
              }
            })
            .done(function(data_s) {
              if (data_s["success"]) {
                console.log(data_s["title"]);
                console.log(data_s["text"].length);
                if (data_s["text"].length != 39) {
                  $(document.getElementById(data_s["title"].replace(/\s/g, '').replace(/['"]+/g, '')), ".container").html(data_s["text"]);
              } else {
                $(document.getElementById(data_s["title"].replace(/\s/g, '').replace(/['"]+/g, ''))).remove();
              }
              } else {
                console.log(data_s["error"]);
              }
            });

        }

      } else {
        $("#content").html("<h6>An unknown error occured, please try again soon</h6>");
        console.log(data["error"]);
      }

    });
}


$(function() {
  $('.centerAll').css({
    'position': 'absolute',
    'left': '50%',
    'top': '50%',
    'margin-left': function() {
      return -$(this).outerWidth() / 2
    },
    'margin-top': function() {
      return -$(this).outerHeight() / 2
    }
  });

  $('.centerBottom').css({
    'position': 'fixed',
    'left': '50%',
    'top': '99%',
    'margin-left': function() {
      return -$(this).outerWidth() / 2
    },
    'margin-top': function() {
      return -$(this).outerHeight() / 2
    }
  });
  scrolly = new SmoothScroll('a[href*="#"]');

  $.ajax('http://localhost:8080/api/tester', {method: "POST"})
      .done(function() {
        getwith("http://localhost:8080/api/headlines", "http://localhost:8080/api/news");
      })
      .fail(function() {
        getwith("https://slim.artifxl.com/api/headlines", "https://slim.artifxl.com/api/news");
      });
});

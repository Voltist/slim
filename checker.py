import requests
import datetime
from pushbullet import Pushbullet
import time
import os

pb = Pushbullet("o.FVrnBC4LuzduKdwxKMkkZOOUSJK30P8p")
push = pb.push_note("Checker Started", "")

doned = False
while True:
    now = datetime.datetime.now()
    if now.hour == 8 and now.minute < 6:
        push = pb.push_note("Checker Operational", "")

    text = ""
    try:
        r = requests.post("https://slim.artifxl.com/api/featured", data={"test": True}, timeout=30)
        if not r.json()["success"]:
            raise ValueError('Error')
    except:
        text += "Featured not operational!"
    try:
        r = requests.post("https://slim.artifxl.com/api/search", data={"search": ".test."}, timeout=30)
        if not r.json()["success"]:
            raise ValueError('Error')
    except:
        text += "\nSearch not operational!"

    if text != "":
        if not doned:
            os.system("sudo service mongod start")
            doned = True
        else:
            push = pb.push_note("ACTION NEEDED!", text)
            doned = False
        print "----"
    else:
        if doned:
            doned = False
            push = pb.push_note("Server Automagically Fixed")
        print "."
    time.sleep(300)

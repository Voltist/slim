import hashlib
import requests
import time
from datetime import datetime
from collections import Counter
import matplotlib.pyplot as plt

password = raw_input("Password: ")
r = requests.post("https://slim.artifxl.com/api/usage", data={"password": password})
data = r.json()["data"]

times = []
for i in data:
    if i["type"] == "featured" and i["success"]:
        times.append(int(i["time"]))

while True:
    ago = int(raw_input("> ")) * 3600
    a = 0
    for i in times:
        if (time.time() - i) < ago:
            a += 1
    print a
    print ""

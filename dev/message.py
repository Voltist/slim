import hashlib
import requests

title = raw_input("Title:\n")
text = raw_input("\nText:\n")
button_text = raw_input("\nButton Text:\n")
button_url = raw_input("\nButton URL:\n")
id = str(hashlib.md5(text).hexdigest())

password = raw_input("\n\n\nPassword: ")
if raw_input("Are you sure? (N/y)").lower() == "y":
    r = requests.post("https://slim.artifxl.com/api/message", data={"title": title, "text": text, "button_text": button_text, "button_url": button_url, "id": id, "password": password})
    print r.text
else:
    print "Aborted!"

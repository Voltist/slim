import hashlib
import requests
import sys
from nltk.corpus import wordnet as wn
from textblob import TextBlob
import re
import wikipedia

from nltk import word_tokenize
from nltk import sent_tokenize
from nltk import pos_tag

def remove_nested_parens(input_str):
    """Returns a copy of 'input_str' with any parenthesized text removed. Nested parentheses are handled."""
    result = ''
    paren_level = 0
    for ch in input_str:
        if ch == '(':
            paren_level += 1
        elif (ch == ')') and paren_level:
            paren_level -= 1
        elif not paren_level:
            result += ch
    return result

def clean(t):
    return t.strip(" ").replace(" ,", ",").replace(" .", ".").replace("$ ", "$").replace(" %", "").replace(".", ". ").replace("( ", "(").replace(" )", ")")

def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

class Article:
    """Retrieves and analyzes wikipedia articles"""

    def __init__(self, text, title):
        self.title = title
        sentences = []
        sent_text = sent_tokenize(clean(text))
        for sentence in sent_text:
            tokenized_text = word_tokenize(remove_nested_parens(sentence))
            sentences.append(pos_tag(tokenized_text))
        self.sentences = sentences
        self.doned = []

        self.banned_words = ["US"]

    def generate_trivia_sentences(self):
        sentences = self.sentences

        trivia_sentences = []
        for sentence in sentences:
            trivia = self.evaluate_sentence(sentence)

            if trivia:
                if len(trivia["answers"]) != 0 and trivia["question"][0] in list("ABCDEFGHIJKLMNOPQRSTUVWXYZ") and "[" not in "".join(trivia["answers"]) and "]" not in "".join(trivia["answers"]):
                    trivia_sentences.append(trivia)

        return trivia_sentences

    def evaluate_sentence(self, sentence):

        tag_map = {word.lower(): tag for word, tag in sentence}

        replace_nouns = []
        for word, tag in sentence:
            # For now, only blank out non-proper nouns that don't appear in the article title
            if (tag == "NNP" or hasNumbers(word)) and word not in self.title and word not in self.banned_words:
                replace_nouns.append(word)

        if len(replace_nouns) < 3:
            # Return none if we found no words to replace
            return None

        answers = []
        s = []
        last = False
        loop = 0
        for i in sentence:
            if (i[0] in replace_nouns or hasNumbers(i[0])) and len(s) > 0:
                if last:
                    answers[-1] = clean(answers[-1] + " " + i[0])
                    tmp = s[-1]
                    s[-1] = tmp.replace("</span>", " " + clean(i[0]) + "</span>")
                else:
                    answers.append(clean(i[0]))
                    s.append(' <span class="answer_box" id="answer_box_' + str(loop) + '">' + clean(i[0]) + '</span>')
                    loop += 1
                last = True
                self.doned.append(clean(i[0]))
            else:
                s.append(i[0])
                last = False

        trivia = {
            'title': self.title,
            'answers': [an.strip(" ") for an in answers],
            'doned': 0,
            "read": False
        }
        trivia['question'] = clean(" ".join(s))

        if trivia['question'].split(" ")[0] not in ["It"] and "_" in trivia['question'].split(",")[0]:
            return trivia
        else:
            return None

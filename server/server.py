from flask import Flask, request
from flask_restful import Resource, Api
from json import dumps
from flask_cors import CORS
from summa import summarizer
import wikipedia as wiki
from newsapi import NewsApiClient
from goose import Goose
import time
import urllib
from HTMLParser import HTMLParser
from pymongo import MongoClient
import hashlib
from learn import Article
import twitter
from bs4 import BeautifulSoup
import requests
import usage
import datetime
import random
import sys, os

app = Flask(__name__)
CORS(app)
api = Api(app)

client = MongoClient('mongodb://admin:admin@localhost:27017/slim')
db = client['slim']
collection = db['usage']
message = db['message']
cache = db['cache']
cache.drop_index("time_1")
cache.ensure_index("time", expireAfterSeconds=604800)

message_database_v2 = db['messagev2']

newsapi = NewsApiClient(api_key='9ae2dd67e65f4298a1a327c8275f1486')
g = Goose()

latest = {}

def gg(data):
    if "guid" not in data.keys():
        return "N/A"
    else:
        return data["guid"]

def verify_pass(passw):
    if str(hashlib.sha256(passw).hexdigest()).lower().strip() == "1a4f6af676ef48affaa2aeb78fd7fb4392b33f32b6b98dc41a22b5b2fb1b6812":
        return True
    else:
        return False

twitter_api = twitter.Api(consumer_key='zy8pqhhWmlhPmg8PgUY0QjpBx',
  consumer_secret='0bEKOb71RKfyVO7XgxnUR1AsTbfVX6IBPEJGZIDX0LLcvi9yQ9',
  access_token_key='949543476722413568-WYYWLE0pBfC6QKN8iRw6iPrsG1GfDfb',
  access_token_secret='ocXXmZiZYjsWBzllje1IWfSBVR6jj4EX5zAzQvP46kNMm')

h = HTMLParser()
def get_featured():
    print "Getting featured..."
    global twitter_api
    t = twitter_api.GetUserTimeline(screen_name="wikipedia", count=50)
    tweets = [i.AsDict() for i in t]
    out = []
    for i in tweets:
        if len(out) < 5:
            try:
                url = i["text"].split("https://")[1]
                r  = requests.get("https://" +url)
                data = r.text
                soup = BeautifulSoup(data)
                out.append(soup.find("h1", {"id": "firstHeading"}).text.strip())
            except (AttributeError, IndexError):
                continue
        else:
            break
    return out

featured = get_featured()
featured_time = int(time.time())


class featured_app(Resource):
    def post(self):
        global featured
        global featured_time
        global collection
        try:
            request_data = request.form.to_dict()
            if (int(time.time()) - featured_time) >= 86400:
                featured = get_featured()
                featured_time = int(time.time())
            if not bool("test" in request_data.keys() and request_data["test"]):
                collection.insert_one({"time": int(time.time()), "ip": request.remote_addr, "type": "featured", "success": True, "guid": gg(request_data)})
            return {"success": True, "titles": featured}
        except Exception as e:
            print e
            collection.insert_one({"time": int(time.time()), "ip": request.remote_addr, "type": "featured", "success": False, "error": str(e)})
            return {"success": False, "error": str(e)}

class search(Resource):
    def post(self):
        try:
            data = request.form.to_dict()
            if "ratio" in data.keys():
                ratio = float(data["ratio"])
            else:
                ratio = 0.25

            if data["search"].strip(" ").lower() == ".test.":
                text = "This is a test. Happy testing!"
                c = "This is a test. Happy testing!"
                title = "Test"
                url = "https://slim.artifxl.com"
                test = True
            else:
                cached = cache.find({"query": data["search"].lower()})
                if cached.count() > 0 and not "pro" in data.keys():
                    print "Using cached..."
                    text = cached[0]["text"]
                    c = cached[0]["bold"]
                    title = cached[0]["title"]
                    url = cached[0]["url"]
                else:
                    print "Searching from wiki..."
                    searched = wiki.search(data["search"])
                    if len(searched) == 1 or not "pro_search" in data.keys():
                        s = wiki.page(searched[0])
                    else:
                        return {"success": True, "options": True, "data": searched}

                    url = s.url
                    content = s.content
                    title = s.title
                    text = summarizer.summarize(content, ratio=ratio)
                    c = summarizer.summarize(content, ratio=ratio/2)
                    cache.insert({"time": datetime.datetime.utcnow(), "text": text, "bold": c, "title": title, "url": url, "query": data["search"].lower()})
                test = False

            out = ""
            bold = ""
            for index, i in enumerate(text.split("\n")):
                if len(i.split(" ")) > 15 or index == 0:
                    if i in c or index == 0:
                        out += "<b>" + i + "</b><br><br>"
                        bold += i + "<br><br>"
                    else:
                        out += i + "<br><br>"

            pro = {}
            if "pro" in data.keys():
                for i in [0.1, 0.2, 0.3, 0.4, 0.5]:
                    tmp = ""
                    text = summarizer.summarize(content, ratio=i)
                    for index, x in enumerate(text.split("\n")):
                        if len(x.split(" ")) > 15 or index == 0:
                            tmp += x + "<br><br>"
                    pro[i] = tmp

            if not test:
                collection.insert_one({"time": int(time.time()), "ip": request.remote_addr, "type": "search", "success": True, "query": data["search"]})
            return {"success": True, "options": False, "result": out, "url": url, "bold": bold, "guid": gg(data),
            "from": "<a onclick='window.open(\"" + url + "\", \"_system\")'>Visit the full article</a>", "pro": pro}
        except Exception as e:
            print e
            collection.insert_one({"time": int(time.time()), "ip": request.remote_addr, "type": "search", "success": False, "error": str(e)})
            return {"success": False, "error": str(e)}


class headlines_app(Resource):
    def post(self):
        global latest
        global time_last_get
        try:
            request_data = request.form.to_dict()
            if request_data["country"] not in latest.keys() or (int(time.time()) - latest[request_data["country"]]["time"]) >= 900:
                print request_data["country"]
                latest[request_data["country"]] = {"data": newsapi.get_top_headlines(country=str(request_data["country"]).lower())["articles"], "time": int(time.time())}
            else:
                print "No new"

            data = []
            for i in latest[request_data["country"]]["data"]:
                data.append({"title": i["title"].rsplit(' - ', 1)[0], "source": i["source"]["name"], "time": i["publishedAt"], "url": i["url"], "urlToImage": i["urlToImage"]})
            if not bool("test" in request_data.keys() and request_data["test"]):
                collection.insert_one({"time": int(time.time()), "ip": request.remote_addr, "type": "headlines", "success": True, "guid": gg(request_data)})
            return {"success": True, "data": data}
        except Exception as e:
            import traceback
            traceback.print_exc()
            return {"success": False}


class get_app(Resource):
    def post(self):
        global latest
        try:
            data = request.form.to_dict()
            for i in latest[data["country"]]["data"]:
                if i["title"].rsplit(' - ', 1)[0] == data["url_id"]:
                    url = i["url"]
                    urlToImage = i["urlToImage"]
                    source = i["source"]["name"]
                    ptime = i["publishedAt"]
                    break
            article = g.extract(url=url)

            text = ""
            s = summarizer.summarize(article.cleaned_text).split("\n")
            for i in s:
                text += "<li class='dlist'><p>" + i + "</p></li>"

            if not bool("test" in data.keys() and data["test"]):
                collection.insert_one({"time": int(time.time()), "ip": request.remote_addr, "type": "get_article", "success": True, "title": data["url_id"].rsplit(' - ', 1)[0], "guid": gg(data)})
            return {"success": True, "text": "<ul>" + text + "</ul>", "title": data["url_id"].rsplit(' - ', 1)[0], "urlToImage": urlToImage, "data": s, "source": source, "time": ptime}
        except Exception as e:
            import traceback
            traceback.print_exc()
            return {"success": False}


class get_sections(Resource):
    def post(self):
        data = request.form.to_dict()
        try:
            try:
                s = wiki.page(data["search"])
            except:
                s = wiki.page(wiki.search(data["search"]))

            secs = s.sections
            del secs[secs.index("References")]
            del secs[secs.index("External links")]

            return {"success": True, "sections": ["Summary"] + secs[:9]}
        except:
            return {"success": False}


class learn_app(Resource):
    def post(self):
        data = request.form.to_dict()
        try:
            try:
                s = wiki.page(data["search"])
            except:
                s = wiki.page(wiki.search(data["search"]))

            if data["section"] == "Summary":
                summary = s.summary.replace("\n\n", "\n").strip("\n")
            else:
                summary = s.content.replace("===", "==").split("\n\n\n== " + data["section"] + " ==\n")[1].split("\n\n\n== ")[0].replace("\n\n", "\n").strip("\n")
                print len(summary.split(" "))

            if len(summary.split(" ")) < 10:
                return {"success": True, "tooshort": True}
            else:
                text0 = summary.split("\n", 1)[0]
                try:
                    text1 = summarizer.summarize(summary.split("\n", 1)[1], words=len(text0.split(" "))).replace("\n", " ")
                except:
                    text1 = ""

                a = Article(text0 + " " + text1, s.title)
                return {"success": True, "tooshort": False, "data": a.generate_trivia_sentences()}

        except Exception, err:
            import traceback
            traceback.print_exc()
            return {"success": False}


class message_app(Resource):
    def post(self):
        try:
            data = request.form.to_dict()
            if verify_pass(data["password"]):
                del data["password"]
                message.insert_one({"time": int(time.time()), "data": data})
                collection.insert_one({"time": int(time.time()), "ip": request.remote_addr, "type": "message", "success": True, "id": data["id"]})
                return {"success": True}
            else:
                return {"success": False, "error": "Incorrect password!!!!!!!"}
        except Exception as e:
            print e
            collection.insert_one({"time": int(time.time()), "ip": request.remote_addr, "type": "message", "success": False, "error": str(e)})
            return {"success": False, "error": str(e)}


class message_get_app(Resource):
    def post(self):
        try:
            data = message.find().sort("time", -1).limit(1)
            collection.insert_one({"time": int(time.time()), "ip": request.remote_addr, "type": "get_message", "success": True})
            return {"success": True, "data": data[0]["data"]}
        except Exception as e:
            print e
            collection.insert_one({"time": int(time.time()), "ip": request.remote_addr, "type": "get_message", "success": False, "error": str(e)})
            return {"success": False, "error": str(e)}

class message_v2(Resource):
    def post(self):
        try:
            request_data = request.form.to_dict()

            options = [{"title": "Please Update", "_id": "N/A"}]
            print request_data["time"]
            for i in message_database_v2.find():
                if float(request_data["time"]) > float(i["min_time"]):
                    options.append(i)
            print options

            choice = random.choice(options)
            del choice["_id"]
            collection.insert_one({"time": int(time.time()), "ip": request.remote_addr, "type": "get_message", "success": True, "choice": choice})
            return {"success": True, "data": choice}
        except Exception as e:
            print e
            collection.insert_one({"time": int(time.time()), "ip": request.remote_addr, "type": "get_message", "success": False, "error": str(e)})
            return {"success": False, "error": str(e)}


class usage_app(Resource):
    def post(self):
        data = request.form.to_dict()
        if verify_pass(data["password"]):
            out = []
            for i in collection.find().sort("time", -1).limit(5000):
                x = i
                del x["_id"]
                out.append(x)
            return {"success": True, "searches": usage.graph(out, "search"), "opened": usage.graph(out, "featured"), "errors": usage.get_errors(out), "guids": usage.get_uniques(out)}
        else:
            return {"success": False, "error": "Incorrect password!!!!!!!"}


class tester(Resource):
    def post(self):
        return {"success": True, "text": "HI!!"}


api.add_resource(headlines_app, '/api/headlines')
api.add_resource(get_app, '/api/news')

api.add_resource(featured_app, '/api/featured')
api.add_resource(search, '/api/search')

api.add_resource(message_app, '/api/message')
api.add_resource(message_get_app, '/api/message_get')
api.add_resource(message_v2, '/api/messagev2')
api.add_resource(usage_app, '/api/usage')

api.add_resource(learn_app, '/api/learn')
api.add_resource(get_sections, '/api/sections')

api.add_resource(tester, '/api/tester')
if __name__ == '__main__':
     app.run(port='8080')

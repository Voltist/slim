from collections import Counter
from datetime import datetime
import time

def graph(data, type):
    times = []
    for i in data:
        if i["type"] == type and i["success"] and not ("test" in i.keys() and i["test"]) and time.time() - i["time"] < 604800:
            times.append(str(datetime.utcfromtimestamp(int(i["time"])).date()))

    dictlist = []
    for key, value in Counter(times).iteritems():
        temp = [key,value]
        dictlist.append(temp)
    return zip(*sorted(dictlist, key=lambda x: x[0]))


def get_errors(data):
    times = []
    for i in data:
        if not i["success"] and not ("test" in i.keys() and i["test"]):
            times.append(i)
    return sorted(times, key=lambda x: x["time"], reverse=True)

def get_uniques(data):
    guids_all = []
    guids_active = []
    for i in data:
        if "guid" in i.keys():
            if time.time() - i["time"] < 604800:
                guids_all.append(i["guid"])
                guids_active.append(i["guid"])
            else:
                guids_all.append(i["guid"])
    return {"all": len(list(set(guids_all))), "active": len(list(set(guids_active)))}
